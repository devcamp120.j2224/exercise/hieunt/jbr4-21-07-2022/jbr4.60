package com.jbr60.customervisitapi;
import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date = new Date();
    private double serviceExpense;
    private double productExpense;

    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    public String getName() {
        return customer.getName();
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double ex) {
        this.serviceExpense = ex;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public void setProductExpense(double ex) {
        this.productExpense = ex;
    }
    public double getTotalExpense() {
        return serviceExpense + productExpense;
    }
    @Override
    public String toString() {
        return "Visit[customer=" + customer
        + ", date=" + date
        + ", productExpense=" + productExpense
        + ", serviceExpense=" + serviceExpense + "]";
    }
}
