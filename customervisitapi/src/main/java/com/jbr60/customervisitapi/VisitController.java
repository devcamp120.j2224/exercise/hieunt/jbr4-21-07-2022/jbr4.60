package com.jbr60.customervisitapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisitController {
    @GetMapping("/visits")
    public static ArrayList<Visit> getVisitList() {
        Customer customer1 = new Customer("Nam");
        Customer customer2 = new Customer("Hoa");
        customer2.setMember(true);
        customer2.setMemberType("Silver");
        Customer customer3 = new Customer("Lan");
        customer3.setMember(true);
        customer3.setMemberType("Gold");
        System.out.println("customer1 là: " + customer1.toString());
        System.out.println("customer2 là: " + customer2.toString());
        System.out.println("customer3 là: " + customer3.toString());
        Date date1 = new Date("03/03/2022");
        Visit visit1 = new Visit(customer1, date1);
        visit1.setServiceExpense(1000);
        visit1.setProductExpense(1500);
        Visit visit2 = new Visit(customer2,new Date());
        visit2.setServiceExpense(1500);
        visit2.setProductExpense(2000);
        Visit visit3 = new Visit(customer3,new Date());
        visit3.setServiceExpense(2500);
        visit3.setProductExpense(3000);
        System.out.println("visit1 là: " + visit1.toString());
        System.out.println("visit2 là: " + visit2.toString());
        System.out.println("visit3 là: " + visit3.toString());
        ArrayList<Visit> visitList = new ArrayList<Visit>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;
    }
}
