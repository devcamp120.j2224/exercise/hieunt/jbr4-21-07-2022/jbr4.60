package com.jbr60.customervisitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomervisitapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomervisitapiApplication.class, args);
		VisitController.getVisitList();
	}

}
